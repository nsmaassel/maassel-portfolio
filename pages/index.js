import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Projects from "../components/Projects";
import Header from "../components/Header";
import Footer from "../components/Footer";

export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>Nick Maassel</title>
                <link rel="icon" href="/favicon.ico"/>
            </Head>
            <main className={styles.main}>
                <Header/>
                <Projects />
            </main>
            <Footer/>
        </div>
    )
}
