export interface IGitlabProjectsData {
    id: number,
    description?: string,
    name: string,
}

export const initGitlabProjectsData: IGitlabProjectsData = {
    id: 10543304,
    description: "",
    name: "cribbage"
}