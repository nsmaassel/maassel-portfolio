import projectStyles from './Project.module.css';

export interface Props {
    project: string;
    // foo?: () => void;
}

const Project = ({project}: Props) => {
    return (
        <div className={projectStyles.project}>
            <div className={projectStyles.icon}>Repo Project Icon goes here</div>
            <div className={projectStyles.title}>{project}</div>
            <div className={projectStyles.description}>This project features these things.</div>
            <div className={projectStyles.tags}>
                <div className={projectStyles.tags}>
                    <div className={projectStyles.tag}>React</div>
                    <div className={projectStyles.tag}>TypeScript</div>
                    <div className={projectStyles.tag}>NextJS</div>
                </div>
            </div>
        </div>
    )
}

export default Project;