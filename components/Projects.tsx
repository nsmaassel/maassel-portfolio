import projectsStyles from './Projects.module.css';
import {useEffect, useState} from "react";
import Project from "./Project";
import {IGitlabProjectsData, initGitlabProjectsData} from "../types/gitlabProjectsData.type";
import axios from "axios";

function Projects() {
    const [gitlabProjectsData, setGitlabProjectsData] = useState([initGitlabProjectsData]);
    const [loading, setLoading] = useState(true);

    // https://docs.gitlab.com/ee/api/#personalproject-access-tokens
    const getGitlabProjectsData = () => {
        axios.get('https://gitlab.com/api/v4/users/nsmaassel/projects'
            , {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Authorization': 'Bearer UK2NPfGTWS588tYJAM6v',
                },
            }
        )
            .then((response) => {
                console.log(response)
                return response.data;
            })
            .then((myJson: IGitlabProjectsData[]) => {
                console.log(myJson);
                setGitlabProjectsData(myJson);
            })
            .catch((error) => {
                    console.log(error)
                }
            )
            .finally(() => setLoading(false));
    }

    useEffect(() => {
        getGitlabProjectsData()
    }, [])

    return (
        <div>
            {!loading &&
            <div className={projectsStyles.projects}>
                {
                    gitlabProjectsData && gitlabProjectsData.length > 0 &&
                    gitlabProjectsData.map(item => <Project key={item.id} project={item.name}/>)
                }
            </div>
            }
            {
                loading &&
                <p>Loading...</p>
            }
        </div>
    );
}

export default Projects;