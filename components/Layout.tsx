import * as React from "react";

import Header from "./Header";
import Footer from "./Footer";

// @ts-ignore
const Layout = ({ children }) => (
    <>
        <Header />
        {children}
        <Footer />
    </>
);

export default Layout;