import headerStyles from './Header.module.css'

function Header() {
    return (
        <div className={headerStyles.Header}>
            <h1 className={headerStyles.title}>
                Welcome to <a href="https://maassel.dev">Maassel.dev!</a>
            </h1>

            <p className={headerStyles.description}>
                Nick Maassel's{' '}
                <code className={headerStyles.code}>portfolio</code>
            </p>
        </div>
    );
}

export default Header;